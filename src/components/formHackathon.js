import React from 'react'

class FormHackathon extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hackathonName: '',
    }

    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(event, value) {
    this.setState({ [event.target.hackathonName]: event.target.value })
  }
  render() {
    return (
      <form>
        <label>
          <h2>DADOS BÁSICOS</h2>
          Nome: &nbsp;
          <input
            type="text"
            name="hackathonName"
            value={this.state.startDateEvent}
            onChange={this.handleChange}
          />
        </label>
      </form>
    )
  }
}

export default FormHackathon
