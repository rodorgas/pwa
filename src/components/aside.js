import Link from 'gatsby-link'
import React, { Component } from 'react'
import IonIcon from 'react-ionicons'

class MenuItem extends Component {
  render() {
    return (
      <Link
        to={this.props.href}
        className="pv3 ph3 mh2 no-underline secondary-color f4 hover-bg-light-gray"
      >
        {this.props.name}
      </Link>
    )
  }
}

class Aside extends Component {
  render() {
    let link = (name, href) => {}

    return (
      <div className="aside" data-activated={this.props.activated}>
        <div className="absolute top-0 bottom-0 left-0 w5 z-2">
          <h2 className="avenir bg-primary light-color ph4 pv3 ma0">
            Hacknizer
          </h2>

          <div className="bg-white pt3 avenir h-100">
            <div className="flex flex-column">
              <MenuItem name="Home" href="#" />
              <MenuItem name="Meu Perfil" href="#" />
              <MenuItem name="Edições" href="#" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Aside
