import React from 'react'
import axios from 'axios'

class EditionsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = { editions: [] }
  }

  async componentDidMount() {
    const allEditions = await axios.post('http://localhost:3000', {
      query: `
        {
          allEditions {
            start_date
            end_date
            Hackathon {
              name
            }
            Pages {
              logo
            }
          }
        }
      `,
      variables: {},
    })
    console.log(allEditions.data.data.allEditions)
    this.setState({ editions: allEditions.data.data.allEditions })
  }

  renderList(list) {
    const newList = list.map(el => (
      <div key={el}>
        <img
          src={el['Pages'][0].logo}
          style={{ width: '50px', height: '50px' }}
        />&nbsp;
        <span>{el['Hackathon'].name}</span>&nbsp;
        <span>{el.start_date}</span>
      </div>
    ))
    return <div>{newList}</div>
  }

  render() {
    return (
      <div>
        <h1>Próximos Hackathons</h1>
        {this.renderList(this.state.editions)}
      </div>
    )
  }
}

export default EditionsList
