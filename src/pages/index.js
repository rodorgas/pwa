import React from 'react'
import Link from 'gatsby-link'
import '../styles/main.scss'
import Banner from '../components/banner'
const IndexPage = () => (
  <div>
    <Banner title="Hacknizer" subTitle="Host the best hackathon" />
  </div>
)

export default IndexPage
